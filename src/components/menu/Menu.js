import React from 'react';
import {Navbar, Nav, NavDropdown, Container} from 'react-bootstrap';

class Menu extends React.Component {
  render() {
  	return (
          <div style={{backgroundColor:'#696666'}} class="container-fluid px-5 py-1">
            <Navbar style={{backgroundColor:'#696666'}} expand="lg" variant="dark">
                <Container class="row">
                    <div class="col-11"></div>
                    <Navbar.Collapse class="col-1">
                        <Nav className="me-auto">
                            <NavDropdown  title="Mi Cuenta">
                            {/* <NavDropdown.Item href="/#/miCuenta">Mi Cuenta</NavDropdown.Item>
                            <NavDropdown.Divider /> */}
                            <NavDropdown.Item href="/">Cerrar Sesión</NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
  	)
  }
}
export default Menu;

