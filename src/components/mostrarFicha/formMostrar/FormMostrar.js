import React from 'react';
import './../../../assets/styles/nav-vertical.css';
import './../../../assets/styles/divs.css';
import $ from 'jquery';
import {Link} from 'react-router-dom';
import {useParams} from 'react-router-dom';
import QRCode from "react-qr-code";
import Image from 'react-bootstrap/image';
window.jQuery = $;

    function FormMostrar(){
        const {id} = useParams()
        const [ficha, setFicha] = React.useState([])



        React.useEffect(() => {
            obtenerDatos()
        },[])

        const obtenerDatos = async() => {
            const data = await fetch(`http://localhost:8080/api/ficha/find/${id}`)
            const fichas = await data.json()
            setFicha(fichas)
        }                                             
        return(
            <div>
                <div>
                    <form>
                        <div id="general" class=""><br/><br/>
                            <div id="formGeneral">
                                <div id="divGeneral" class="row form-group">
                                    <div class="col">
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left" for="LastName">Codigo:</label>
                                            <div class="col">
                                                <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                    {ficha.id}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row orm-group">
                                            <label class="col control-label" for="FirstName">Linea:</label>
                                            <div class="col">
                                                <label class="form-control borde text-center" id="FirstName" name="FirstName" type="text">
                                                    {ficha.linea} 
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row form-group py-2">
                                            <label class="col control-label pull-left" for="LastName">Serie:</label>
                                            <div class="col">
                                                <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                    {ficha.serie}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left" for="LastName">Altura de Taco:</label>
                                            <div class="col">
                                                <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                    {ficha.alTaco}
                                                </label>
                                            </div>
                                        </div>                                
                                    </div>

                                    <div class="col colum2">
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left" for="LastName">Color:</label>
                                            <div class="col">
                                                <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                    {ficha.color}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left" for="LastName">Estilo:</label>
                                            <div class="col">
                                                <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                    {ficha.estilo}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left" for="LastName">Codigo de Horma:</label>
                                            <div class="col">
                                                <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                    {ficha.codHorma}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left" for="LastName">Codigo de Planta:</label>
                                            <div class="col">
                                                <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                    {ficha.codPlanta}
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group py-1">
                                            <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="control-label pull-left labelTil" for="LastName"><h5 id="codigoQR"><b>Codigo QR</b></h5></label>
                                            <div class="qr">
                                                <QRCode value={`${id}`} size="128" level="M"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><br/>

                        <div>
                            <div id="" class="px-5 py-4 text-center">
                                <h2 class="py-2" style={{ color: "#A46F1F" }} ><b>Especificaciones Tecnicas</b></h2>
                            </div>

                            <div id="formEspecificaciones1">
                                <div>
                                    <div id="Espe1" class="row">
                                        <div id="apartado1" class="col">
                                            <div class="row">
                                                <div class="col-3">
                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Material</b></label>
                                                    </div>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Cuero 1</label>
                                                    </div>
                                                    
                                                    <div id="cuero2" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Cuero 2</label>
                                                    </div>

                                                    <div id="cuero3" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Cuero 3</label>
                                                    </div><br/>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Forro</b></label>
                                                    </div>
                                                    
                                                    <div id="cuero2" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Forro 1</label>
                                                    </div>

                                                    <div id="cuero3" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Forro 2</label>
                                                    </div>                                                
                                                </div>

                                                <div class="col contenedor1">
                                                    <div class="form-group py-1">
                                                        <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                            <h5 id="codigoQR" class="col"><b>Tipo</b></h5>
                                                            <h5 id="codigoQR" class="col"><b>Color</b></h5>
                                                        </label>
                                                    </div>
                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                                {ficha.c1Tipo}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                {ficha.c1Color}
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                                {ficha.c2Tipo}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                {ficha.c2Color}
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                                {ficha.c3Tipo}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                {ficha.c3Color}
                                                            </label>
                                                        </div>
                                                    </div><br/><br/>

                                                    <div class="row form-group py-1 forro">
                                                        <div class="col">
                                                            <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                                {ficha.f1Tipo}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                {ficha.f1Color}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group py-1">
                                                    <div class="col">
                                                            <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                                {ficha.f2Tipo}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                {ficha.f2Color}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="img1" class="col">
                                            <div>
                                                <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="labelImg1" for="LastName">
                                                    <h5 id="codigoQR"><b>Imagen 1</b></h5>
                                                </label>
                                                <div id="divImg2">
                                                    <img src={ficha.imgRuta1}></img>
                                                </div>
                                            </div>                          
                                        </div>
                                    </div><br/><br/>

                                    <div id="Espe2" class="row">
                                        <div id="apartado2" class="col">
                                            <div id="Hilo" class="">
                                                <div class="row">
                                                    <div class="col-3">
                                                        <div id="cuero" class="form-group py-1">
                                                            <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Hilo</b></label>
                                                        </div>

                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Hilo 1</label>
                                                        </div>
                                                        
                                                        <div id="cuero2" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Hilo 2</label>
                                                        </div>

                                                        <div id="cuero3" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Hilo 3</label>
                                                        </div>                                                                                        
                                                    </div>

                                                    <div class="col contenedor1">
                                                        <div class="form-group py-1">
                                                            <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                                <h5 id="codigoQR" class="col"><b>Tipo</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Número</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Color</b></h5>
                                                            </label>
                                                        </div>
                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                                    {ficha.h1Tipo}
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="form-control borde conten2 text-center" id="" name="LastName" type="text">
                                                                    {ficha.h1Numero}
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                    {ficha.h1Color}
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                                    {ficha.h2Tipo}
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="form-control borde conten2 text-center" id="" name="LastName" type="text">
                                                                    {ficha.h2Numero}
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                    {ficha.h2Color}
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                                    {ficha.h3Tipo}
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="form-control borde conten2 text-center" id="" name="LastName" type="text">
                                                                    {ficha.h3Numero}
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                    {ficha.h3Color}
                                                                </label>
                                                            </div>
                                                        </div><br/>                      
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3">
                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Accesorios</b></label>
                                                    </div>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Accesorios 1</label>
                                                    </div>
                                                    
                                                    <div id="cuero2" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Accesorios 2</label>
                                                    </div>

                                                    <div id="cuero3" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Accesorios 3</label>
                                                    </div><br/>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Cierre</b></label>
                                                    </div>
                                                    
                                                    <div id="cuero2" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Cierre</label>
                                                    </div>

                                                    <div id="cuero3" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Puller</label>
                                                    </div>                                                
                                                </div>

                                                <div class="col contenedor1">
                                                    <div class="form-group py-1">
                                                        <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                            <h5 id="codigoQR" class="col"><b>Detalle</b></h5>
                                                            <h5 id="codigoQR" class="col"><b>Material</b></h5>
                                                            <h5 id="codigoQR" class="col"><b>Color</b></h5>
                                                        </label>
                                                    </div>
                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                                {ficha.acc1Detalle}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten2 text-center" id="" name="LastName" type="text">
                                                                {ficha.acc1Material}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                {ficha.acc1Color}
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                                {ficha.acc2Detalle}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten2 text-center" id="" name="LastName" type="text">
                                                                {ficha.acc2Material}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                {ficha.acc2Color}
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                                {ficha.acc3Detalle}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten2 text-center" id="" name="LastName" type="text">
                                                                {ficha.acc3Material}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                {ficha.acc3Color}
                                                            </label>
                                                        </div>
                                                    </div><br/><br/><br/>

                                                    <div class="row form-group py-1 forro">
                                                        <div class="col">
                                                            <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                                {ficha.cierreDetalle}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten2 text-center" id="" name="LastName" type="text">
                                                                {ficha.cierreMaterial}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                {ficha.cierreColor}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                                {ficha.pullerDetalle}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten2 text-center" id="" name="LastName" type="text">
                                                                {ficha.pullerMaterial}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                {ficha.pullerColor}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="img2" class="col">
                                            <div>
                                                <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="labelImg1" for="LastName">
                                                    <h5 id="codigoQR"><b>Imagen 2</b></h5>
                                                </label>
                                            </div>
                                            <div id="divImg2">
                                                <img></img>
                                            </div>                                                                                 
                                        </div>
                                    </div><br/><br/>

                                    <div id="Espe3" class="row">
                                        <div id="apartado3" class="col">
                                            <div id="Hilo" class="col">
                                                <div class="row">
                                                    <div class="col-3">
                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName"></label>
                                                        </div> 
                                                        <div id="cuero" class="form-group py-1">
                                                            <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Puntera</b></label>
                                                        </div>

                                                        <div id="cuero" class="form-group py-1">
                                                            <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Contrafuerte</b></label>
                                                        </div>                                                                                 
                                                    </div>

                                                    <div class="col contenedor1">
                                                        <div class="form-group py-1">
                                                            <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo">
                                                                <h5 id="codigoQR" class="col"><b>Marerial</b></h5>
                                                            </label>
                                                        </div>
                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <label id="" class="form-control borde contra text-center" name="LastName" type="text">
                                                                    {ficha.punteraMaterial}
                                                                </label>
                                                            </div>                                                
                                                        </div>

                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <label id="" class="form-control borde contra text-center" name="LastName" type="text">
                                                                    {ficha.contraMaterial}
                                                                </label>
                                                            </div>                                                        
                                                        </div><br/>                      
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3">
                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Piso</b></label>
                                                    </div>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Piso 1</label>
                                                    </div>
                                                    
                                                    <div id="cuero2" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Piso 2</label>
                                                    </div>

                                                    <div id="cuero3" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Plataforma</label>
                                                    </div>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Taco</label>
                                                    </div>                                         
                                                </div>

                                                <div class="col contenedor1">
                                                    <div class="form-group py-1">
                                                        <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                            <h5 id="codigoQR" class="col"><b>Material</b></h5>
                                                            <h5 id="codigoQR" class="col">Color<b></b></h5>
                                                            <h5 id="codigoQR" class="col"><b>Forrado</b></h5>
                                                        </label>
                                                    </div>
                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                                {ficha.p1Material}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten2 text-center" id="" name="LastName" type="text">
                                                                {ficha.p1Color}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                {ficha.p1Forrado}
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                                {ficha.p2Material}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten2 text-center" id="" name="LastName" type="text">
                                                                {ficha.p2Color}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                {ficha.p2Forrado}
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                                {ficha.plataformaMaterial}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten2 text-center" id="" name="LastName" type="text">
                                                                {ficha.plataformaColor}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                {ficha.plataformaForrado}
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                                {ficha.tacoMaterial}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten2 text-center" id="" name="LastName" type="text">
                                                                {ficha.tacoColor}
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                {ficha.tacoForrado}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="img3" class="col">
                                            <div>
                                                <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="labelImg1" for="LastName">
                                                    <h5 id="codigoQR"><b>Imagen 3</b></h5>
                                                </label>
                                            </div>
                                            <div id="divImg2">
                                                <img></img>
                                            </div>                                                                                 
                                        </div>
                                    </div><br/><br/>

                                    <div id="Espe4" class="row">
                                        <div id="apartado4" class="col">
                                            <div id="Plantilla" class="col">
                                                <div class="row">
                                                    <div class="col-3">
                                                        <div id="cuero" class="form-group py-1">
                                                            <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Plantilla</b></label>
                                                        </div>

                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Plantilla</label>
                                                        </div>
                                                        
                                                        <div id="cuero2" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Acolche</label>
                                                        </div>                        
                                                    </div>

                                                    <div class="col contenedor1">
                                                        <div class="form-group py-1">
                                                            <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                                <h5 id="codigoQR" class="col"><b>Material</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Color</b></h5>
                                                            </label>
                                                        </div>
                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                                    {ficha.plantillaMaterial}
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                    {ficha.plantillaColor}
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <div class="form-group py-1">
                                                            <div class="">
                                                                <label class="form-control borde plantillaImp text-center" id="" name="LastName" type="text">
                                                                    {ficha.acolcheMaterial}
                                                                </label>
                                                            </div>
                                                        </div>                                                   
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="costura" class="">
                                                <div class="row">
                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Costura Plantilla</b></label>
                                                    </div>
                                                    <div class="col-3">
                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName"></label>
                                                        </div>
                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Hilo</label>
                                                        </div>
                                                        
                                                        <div id="cuero2" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Costura</label>
                                                        </div>                                                                                      
                                                    </div>

                                                    <div class="col contenedor1">
                                                        <div class="form-group py-1">
                                                            <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                                <h5 id="codigoQR" class="col"><b>Tipo</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Número</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Color</b></h5>
                                                            </label>
                                                        </div>
                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                                    {ficha.h4Tipo}
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="form-control borde conten2 text-center" id="" name="LastName" type="text">
                                                                    {ficha.h4Numero}
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                    {ficha.h4Color}
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <div class="form-group py-1">
                                                            <div class="">
                                                                <label class="form-control borde CostAcceImp text-center" id="" name="LastName" type="text">
                                                                    {ficha.costuraTipo}
                                                                </label>
                                                            </div>
                                                        </div>                      
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="Accesorios" class="">
                                                <div class="row">
                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Accesorios de Acabado</b></label>
                                                    </div>
                                                    <div class="col-3">
                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName"></label>
                                                        </div>

                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Sello</label>
                                                        </div>
                                                        
                                                        <div id="cuero2" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Cuerito</label>
                                                        </div>

                                                        <div id="cuero3" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Han Tag</label>
                                                        </div>                                                                                        
                                                    </div>

                                                    <div class="col contenedor1">
                                                        <div class="form-group py-1">
                                                            <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                                <h5 id="codigoQR" class="col"><b>Marca</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Tipo</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Material</b></h5>
                                                            </label>
                                                        </div>
                                                        <div class="row py-1">
                                                            <div class="col">
                                                                <label class="form-control borde text-center" id="" name="LastName" type="text">
                                                                    {ficha.selloMarca}
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="form-control borde conten2 text-center acabado" id="" name="LastName" type="text">
                                                                    {ficha.selloTipo}
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                    {ficha.selloMaterial}
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <div class="row py-1">
                                                            <div class="col">
                                                                <label class="form-control borde text-center acabadocuero" id="" name="LastName" type="text">
                                                                    {ficha.cueritoMarca}
                                                                </label>
                                                            </div>
                                                            <div class="col"> 
                                                                <label class="form-control borde conten2 acabado text-center" id="" name="LastName" type="text">
                                                                    {ficha.cueritoTipo}
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="form-control borde conten1 text-center" id="" name="LastName" type="text">
                                                                    {ficha.cueritoMaterial}       
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <div class="py-1">
                                                            <div class="">
                                                                <label class="borde CostAcceImp text-center" id="" name="LastName" type="text">
                                                                    {ficha.hantagMarca}
                                                                </label>
                                                            </div>
                                                        </div><br/>                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="img4" class="col">
                                            <div>
                                                <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="labelImg1" for="LastName">
                                                    <h5 id="codigoQR"><b>Imagen 4</b></h5>
                                                </label>
                                            </div>
                                            <div id="divImg2">
                                                <img id="divImg2"></img>
                                            </div>                                                                                                                            
                                        </div>
                                    </div><br/>
                                </div>
                            </div>                        
                        </div>
                    </form><br/>
                    <div>
                        <div id="botonesFormDiv">
                            <div  class="row text-center container">
                                <div class="col">
                                    <Link to={`/mostrarFicha/editarFicha/${ficha.id}`}>
                                        <a id="margenbotom1">
                                        <input type="button" class="btn botonesForm" style={{backgroundColor:'#696666', color:'#ffffff' }} value="Editar"></input></a>
                                    </Link>
                                </div>
                                <div class="col">
                                    <a id="margenbotom2" href="/#/fichas">
                                    <input  type="button" class="btn botonesForm" style={{backgroundColor:'#696666', color:'#ffffff' }} value="Cancelar"></input></a>
                                </div>
                            </div>
                        </div>
                    </div><br/>
                    <div class="botonesFormDiv1">
                        <div class="text-center container">
                            <a id="margenbotom3" href="">
                            <input  type="button" class="btn botonesForm1" style={{backgroundColor:'#696666', color:'#ffffff' }} value="Exportar a Pdf"></input></a>
                        </div>
                    </div><br/><br/>
                </div>
            </div>
        )
    }
export default FormMostrar;