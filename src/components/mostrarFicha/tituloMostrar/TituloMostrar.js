import React from 'react';
import './../../../assets/styles/divs.css';
import './../../../assets/styles/nav-vertical.css';
import './../../../assets/styles/divs.css';
import logolat from './../../../assets/img/logolat.png';
import $ from 'jquery';
window.jQuery = $;

class TituloMostrar extends React.Component {
    componentDidMount() {
        $('#mostrar-nav').on('click',function(){
            $('.nav-lat').toggleClass('mostrar');
        });
        $('#cerrar-nav').on('click',function(){
            $('.nav-lat').toggleClass('mostrar');
        });
    }
    render(){
        return(
            <div id="divTitulo" style={{backgroundColor:'#696666'}} class="px-5 py-1 text-center">
                <div id="mostrar-nav" class="body"></div>
                    <nav class="nav-lat">
                        <div id="cerrar-nav"></div>
                        
                        <div><img src={logolat} class="logo"></img></div>
                        
                            <ul class="menu">
                                <li><a href="/#/home"><b>inicio</b></a></li>
                                <li><a href="/#/fichas"><b>Ficha Tecnica</b></a></li>
                                <li><a href="#"><b>Servicios</b></a></li>
                            </ul>
                    </nav>
                <h2 class="py-2" style={{ color: "#FFFFFF" }} ><b>Ficha Tecnica</b></h2>
            </div>
        )
    }
}
export default TituloMostrar;