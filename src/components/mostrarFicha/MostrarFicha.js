import React from 'react';
import TituloMostrar from './tituloMostrar/TituloMostrar';
import FormMostrar from './formMostrar/FormMostrar';


class MostrarFicha extends React.Component {
    render(){
        return(
            <>
            <TituloMostrar/> 
			<main role="main" class="">
                <div class="">
				    <FormMostrar />
                </div>	
	  		</main>
	  		</>
        )
    }
}
export default MostrarFicha;