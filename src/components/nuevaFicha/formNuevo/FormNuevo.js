import React, { useState } from 'react';
import './../../../assets/styles/nav-vertical.css';
import './../../../assets/styles/divs.css';
import logolat from './../../../assets/img/logolat.png';
import { ConsultaService } from '../../../service/ConsultaService';
import $ from 'jquery';
window.jQuery = $;

class FormNuevo extends React.Component {
    constructor(){
        super();
        this.state = {
            ficha: {
            id: null,
            linea: null,
            serie: null,
            alTaco: null,
            color: null,
            estilo: null,
            codHorma: null,
            codPlanta: null,
            c1Tipo: null,
            c1Color: null,
            c2Tipo: null,
            c2Color: null,
            c3Tipo: null,
            c3Color: null,
            f1Tipo: null,
            f1Color: null,
            f2Tipo: null,
            f2Color: null,
            h1Tipo: null,
            h1Numero: null,
            h1Color: null,
            h2Tipo: null,
            h2Numero: null,
            h2Color: null,
            h3Tipo: null,
            h3Numero: null,
            h3Color: null,
            acc1Detalle: null,
            acc1Material: null,
            acc1Color: null,
            acc2Detalle: null,
            acc2Material: null,
            acc2Color: null,
            acc3Detalle: null,
            acc3Material: null,
            acc3Color: null,
            cierreDetalle: null,
            cierreMaterial: null,
            cierreColor: null,
            pullerDetalle: null,
            pullerMaterial: null,
            pullerColor: null,
            punteraMaterial: null,
            contraMaterial: null,
            p1Material: null,
            p1Color: null,
            p1Forrado: null,
            p2Material: null,
            p2Color: null,
            p2Forrado: null,
            plataformaMaterial: null,
            plataformaColor: null,
            plataformaForrado: null,
            tacoMaterial: null,
            tacoColor: null,
            tacoForrado: null,
            plantillaMaterial: null,
            plantillaColor: null,
            acolcheMaterial: null,
            h4Tipo: null,
            h4Numero: null,
            h4Color: null,
            costuraTipo: null,
            selloMarca: null,
            selloTipo: null,
            selloMaterial: null,
            cueritoMarca: null,
            cueritoTipo: null,
            cueritoMaterial: null,
            hantagMarca: null,
            imgRuta1: null
          },
          selectedFicha : {
    
          }
        };
        this.consultaService = new ConsultaService();
        this.save = this.save.bind(this);
      }
    
    
    componentDidMount() {
        this.consultaService.getAll().then(data => this.setState({fichas: data}))

        $('#mostrar-nav').on('click',function(){
            $('.nav-lat').toggleClass('mostrar');
        });
        $('#cerrar-nav').on('click',function(){
            $('.nav-lat').toggleClass('mostrar');
        });
    }
    
    


    save(){
        this.consultaService.save(this.state.ficha).then(data =>{
          this.setState({
            ficha: {
                id: null,
                linea: null,
                serie: null,
                alTaco: null,
                color: null,
                estilo: null,
                codHorma: null,
                codPlanta: null,
                c1Tipo: null,
                c1Color: null,
                c2Tipo: null,
                c2Color: null,
                c3Tipo: null,
                c3Color: null,
                f1Tipo: null,
                f1Color: null,
                f2Tipo: null,
                f2Color: null,
                h1Tipo: null,
                h1Numero: null,
                h1Color: null,
                h2Tipo: null,
                h2Numero: null,
                h2Color: null,
                h3Tipo: null,
                h3Numero: null,
                h3Color: null,
                acc1Detalle: null,
                acc1Material: null,
                acc1Color: null,
                acc2Detalle: null,
                acc2Material: null,
                acc2Color: null,
                acc3Detalle: null,
                acc3Material: null,
                acc3Color: null,
                cierreDetalle: null,
                cierreMaterial: null,
                cierreColor: null,
                pullerDetalle: null,
                pullerMaterial: null,
                pullerColor: null,
                punteraMaterial: null,
                contraMaterial: null,
                p1Material: null,
                p1Color: null,
                p1Forrado: null,
                p2Material: null,
                p2Color: null,
                p2Forrado: null,
                plataformaMaterial: null,
                plataformaColor: null,
                plataformaForrado: null,
                tacoMaterial: null,
                tacoColor: null,
                tacoForrado: null,
                plantillaMaterial: null,
                plantillaColor: null,
                acolcheMaterial: null,
                h4Tipo: null,
                h4Numero: null,
                h4Color: null,
                costuraTipo: null,
                selloMarca: null,
                selloTipo: null,
                selloMaterial: null,
                cueritoMarca: null,
                cueritoTipo: null,
                cueritoMaterial: null,
                hantagMarca: null,
                imgRuta1: null
            }
        });
        })
      }

    render(){
        return(
            <div>
                <div id="mostrar-nav" class="body"></div>
                    <nav class="nav-lat">
                        <div id="cerrar-nav"></div>
                        
                        <div><img src={logolat} class="logo"></img></div>
                        
                            <ul class="menu">
                                <li><a href="/#/home"><b>inicio</b></a></li>
                                <li><a href="/#/fichas"><b>Ficha Tecnica</b></a></li>
                                <li><a href="#"><b>Servicios</b></a></li>
                            </ul>
                    </nav>
                <div>
                    <form id="ficha-form">
                        <div id="general" class=""><br/><br/>
                            <div id="formGeneral">
                                <div id="divGeneral" class="row form-group">
                                    <div class="col">
                                        <div class="row orm-group">
                                            <label class="col control-label">Linea:</label>
                                            <div class="col">
                                                <input class="form-control borde" id="linea" type="text" value={this.state.ficha.linea} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let ficha = Object.assign({}, prevState.ficha);
                                                        ficha.linea = val;
                                                        
                                                        return {ficha};
                                                    })}}
                                                 />
                                            </div>
                                        </div>
                                        <div class="row form-group py-2">
                                            <label class="col control-label pull-left" >Serie:</label>
                                            <div class="col">
                                                <input class="form-control borde" id="serie" type="text" value={this.state.ficha.serie} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let ficha = Object.assign({}, prevState.ficha);
                                                        ficha.serie = val;
                                                        
                                                        return {ficha};
                                                    })}}
                                                 />
                                            </div>
                                        </div>
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left">Altura de Taco:</label>
                                            <div class="col">
                                                <input class="form-control borde" id="alTaco" type="number" value={this.state.ficha.alTaco} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let ficha = Object.assign({}, prevState.ficha);
                                                        ficha.alTaco = val;
                                                        
                                                        return {ficha};
                                                    })}}
                                                 />                                             
                                            </div>
                                        </div>
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left">Color:</label>
                                            <div class="col">
                                                <input class="form-control borde" id="color" type="text" value={this.state.ficha.color} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let ficha = Object.assign({}, prevState.ficha);
                                                        ficha.color = val;
                                                        
                                                        return {ficha};
                                                    })}}
                                                 />
                                            </div>
                                        </div>                                
                                    </div>

                                    <div class="col colum2">
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left">Estilo:</label>
                                            <div class="col">
                                                <input class="form-control borde" id="estilo" type="text" value={this.state.ficha.estilo} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let ficha = Object.assign({}, prevState.ficha);
                                                        ficha.estilo = val;
                                                        
                                                        return {ficha};
                                                    })}}
                                                 />
                                            </div>
                                        </div>
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left">Codigo de Horma:</label>
                                            <div class="col">
                                                <input class="form-control borde" id="codHorma" type="text" value={this.state.ficha.codHorma} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let ficha = Object.assign({}, prevState.ficha);
                                                        ficha.codHorma = val;
                                                        
                                                        return {ficha};
                                                    })}}
                                                 />
                                            </div>
                                        </div>
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left">Codigo de Planta:</label>
                                            <div class="col">
                                                <input class="form-control borde" id="codPlanta" type="text" value={this.state.ficha.codPlanta} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let ficha = Object.assign({}, prevState.ficha);
                                                        ficha.codPlanta = val;
                                                        
                                                        return {ficha};
                                                    })}}
                                                 />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group py-1"></div>
                                    </div>
                                </div>
                            </div>
                        </div><br/><br/>

                        <div id="formEspecificaciones">
                            <div>
                                <div id="" class="px-5 py-4 text-center">
                                    <h2 class="py-2" style={{ color: "#A46F1F" }} ><b>Especificaciones Tecnicas</b></h2>
                                </div><br/>

                                <div>
                                    <div id="Espe1" class="row container">
                                        <div id="apartado1" class="col">
                                            <div class="row">
                                                <div class="col-3">
                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5"><b>Material</b></label>
                                                    </div>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label class="control-label pull-left">Cuero 1</label>
                                                    </div>
                                                    
                                                    <div id="cuero2" class="form-group py-1">
                                                        <label class="control-label pull-left">Cuero 2</label>
                                                    </div>

                                                    <div id="cuero3" class="form-group py-1">
                                                        <label class="control-label pull-left">Cuero 3</label>
                                                    </div><br/>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5"><b>Forro</b></label>
                                                    </div>
                                                    
                                                    <div id="cuero2" class="form-group py-1">
                                                        <label class="control-label pull-left">Forro 1</label>
                                                    </div>

                                                    <div id="cuero3" class="form-group py-1">
                                                        <label class="control-label pull-left">Forro 2</label>
                                                    </div>                                                
                                                </div>

                                                <div class="col contenedor1">
                                                    <div class="form-group py-1">
                                                        <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                            <h5 id="codigoQR" class="col"><b>Tipo</b></h5>
                                                            <h5 id="codigoQR" class="col"><b>Color</b></h5>
                                                        </label>
                                                    </div>
                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="c1Tipo" type="text" value={this.state.ficha.c1Tipo} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.c1Tipo = val;
                                                        
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="c1Color" type="text" value={this.state.ficha.c1Color} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.c1Color = val;
                                                        
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="c2Tipo" type="text" value={this.state.ficha.c2Tipo} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.c2Tipo = val;
                                                        
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="c2Color" type="text" value={this.state.ficha.c2Color} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.c2Color = val;
                                                        
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="c3Tipo" type="text" value={this.state.ficha.c3Tipo} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.c3Tipo = val;
                                                        
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="c3Color" type="text" value={this.state.ficha.c3Color} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.c3Color = val;
                                                        
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                    </div><br/><br/>

                                                    <div class="row form-group py-1 forro">
                                                        <div class="col">
                                                            <input class="form-control borde" id="f1Tipo" type="text" value={this.state.ficha.f1Tipo} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.f1Tipo = val;
                                                        
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="f1Color" type="text" value={this.state.ficha.f1Color} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.f1Color = val;
                                                        
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1 forro">
                                                        <div class="col">
                                                            <input class="form-control borde" id="f2Tipo" type="text" value={this.state.ficha.f2Tipo} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.f2Tipo = val;
                                                        
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="f2Color" type="text" value={this.state.ficha.f2Color} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.f2Color = val;
                                                        
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="img1" class="col">
                                            <div>
                                                <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="labelImg" for="LastName">
                                                    <h5 id="codigoQR"><b>Imagen 1</b></h5>
                                                </label>
                                            </div>
                                            <div id="divImg1">
                                                <img></img>
                                            </div>
                                            <div class="text-center">
                                                <input type="file" class="btn botonImg"  id="imgRuta1" style={{backgroundColor:'#a46f1f', color:'#ffffff' }} value={this.state.ficha.imgRuta1} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.imgRuta1 = val;
                                                        
                                                                    return {ficha};
                                                                })}}
                                                            />
                                            </div>                                       
                                        </div>
                                    </div><br/><br/>

                                    <div id="Espe2" class="row container">
                                        <div id="apartado2" class="col">
                                            <div id="Hilo" class="">
                                                <div class="row">
                                                    <div class="col-3">
                                                        <div id="cuero" class="form-group py-1">
                                                            <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Hilo</b></label>
                                                        </div>

                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Hilo 1</label>
                                                        </div>
                                                        
                                                        <div id="cuero2" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Hilo 2</label>
                                                        </div>

                                                        <div id="cuero3" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Hilo 3</label>
                                                        </div>                                                                                        
                                                    </div>

                                                    <div class="col contenedor1">
                                                        <div class="form-group py-1">
                                                            <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                                <h5 id="codigoQR" class="col"><b>Tipo</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Número</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Color</b></h5>
                                                            </label>
                                                        </div>
                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <input class="form-control borde" id="h1Tipo" type="text" value={this.state.ficha.h1Tipo} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.h1Tipo = val;
                                                        
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten2" id="h1Numero" type="number" value={this.state.ficha.h1Numero} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.h1Numero = val;
                                                        
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten1" id="h1Color" type="text" value={this.state.ficha.h1Color} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.h1Color = val;
                                                        
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                        </div>

                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <input class="form-control borde" id="h2Tipo" type="text" value={this.state.ficha.h2Tipo} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.h2Tipo = val;
                                                        
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten2" id="h2Numero" type="number" value={this.state.ficha.h2Numero} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.h2Numero = val;
                                                        
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten1" id="h2Color" type="text" value={this.state.ficha.h2Color} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.h2Color = val;
                                                        
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                        </div>

                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <input class="form-control borde" id="h3Tipo" type="text" value={this.state.ficha.h3Tipo} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.h3Tipo = val;
                                                        
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten2" id="h3Numero" type="number" value={this.state.ficha.h3Numero} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.h3Numero = val;
                                                        
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten1" id="h3Color" type="text" value={this.state.ficha.h3Color} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.h3Color = val;
                                                        
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                        </div><br/>                      
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3">
                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Accesorios</b></label>
                                                    </div>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Accesorios 1</label>
                                                    </div>
                                                    
                                                    <div id="cuero2" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Accesorios 2</label>
                                                    </div>

                                                    <div id="cuero3" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Accesorios 3</label>
                                                    </div><br/>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Cierre</b></label>
                                                    </div>
                                                    
                                                    <div id="cuero2" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Cierre</label>
                                                    </div>

                                                    <div id="cuero3" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Puller</label>
                                                    </div>                                                
                                                </div>

                                                <div class="col contenedor1">
                                                    <div class="form-group py-1">
                                                        <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                            <h5 id="codigoQR" class="col"><b>Detalle</b></h5>
                                                            <h5 id="codigoQR" class="col"><b>Material</b></h5>
                                                            <h5 id="codigoQR" class="col"><b>Color</b></h5>
                                                        </label>
                                                    </div>
                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="acc1Detalle" type="text" value={this.state.ficha.acc1Detalle} onChange={(e) => {
                                                            let val = e.target.value;
                                                            this.setState(prevState => {
                                                                let ficha = Object.assign({}, prevState.ficha);
                                                                ficha.acc1Detalle = val;
                                                    
                                                                return {ficha};
                                                            })}}
                                                        />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten2" id="acc1Material" type="text" value={this.state.ficha.acc1Material} onChange={(e) => {
                                                            let val = e.target.value;
                                                            this.setState(prevState => {
                                                                let ficha = Object.assign({}, prevState.ficha);
                                                                ficha.acc1Material = val;
                                                    
                                                                return {ficha};
                                                            })}}
                                                        />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="acc1Color" type="text" value={this.state.ficha.acc1Color} onChange={(e) => {
                                                            let val = e.target.value;
                                                            this.setState(prevState => {
                                                                let ficha = Object.assign({}, prevState.ficha);
                                                                ficha.acc1Color = val;
                                                    
                                                                return {ficha};
                                                            })}}
                                                        />
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="acc2Detalle" type="text" value={this.state.ficha.acc2Detalle} onChange={(e) => {
                                                            let val = e.target.value;
                                                            this.setState(prevState => {
                                                                let ficha = Object.assign({}, prevState.ficha);
                                                                ficha.acc2Detalle = val;
                                                    
                                                                return {ficha};
                                                            })}}
                                                        />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten2" id="acc2Material" type="text" value={this.state.ficha.acc2Material} onChange={(e) => {
                                                            let val = e.target.value;
                                                            this.setState(prevState => {
                                                                let ficha = Object.assign({}, prevState.ficha);
                                                                ficha.acc2Material = val;
                                                    
                                                                return {ficha};
                                                            })}}
                                                        />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="acc2Color" type="text" value={this.state.ficha.acc2Color} onChange={(e) => {
                                                            let val = e.target.value;
                                                            this.setState(prevState => {
                                                                let ficha = Object.assign({}, prevState.ficha);
                                                                ficha.acc2Color = val;
                                                    
                                                                return {ficha};
                                                            })}}
                                                        />
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="acc3Detalle" type="text" value={this.state.ficha.acc3Detalle} onChange={(e) => {
                                                            let val = e.target.value;
                                                            this.setState(prevState => {
                                                                let ficha = Object.assign({}, prevState.ficha);
                                                                ficha.acc3Detalle = val;
                                                    
                                                                return {ficha};
                                                            })}}
                                                        />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten2" id="acc3Material" type="text" value={this.state.ficha.acc3Material} onChange={(e) => {
                                                            let val = e.target.value;
                                                            this.setState(prevState => {
                                                                let ficha = Object.assign({}, prevState.ficha);
                                                                ficha.acc3Material = val;
                                                    
                                                                return {ficha};
                                                            })}}
                                                        />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="acc3Color" type="text" value={this.state.ficha.acc3Color} onChange={(e) => {
                                                            let val = e.target.value;
                                                            this.setState(prevState => {
                                                                let ficha = Object.assign({}, prevState.ficha);
                                                                ficha.acc3Color = val;
                                                    
                                                                return {ficha};
                                                            })}}
                                                        />
                                                        </div>
                                                    </div><br/><br/><br/>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="cierreDetalle" type="text" value={this.state.ficha.cierreDetalle} onChange={(e) => {
                                                            let val = e.target.value;
                                                            this.setState(prevState => {
                                                                let ficha = Object.assign({}, prevState.ficha);
                                                                ficha.cierreDetalle = val;
                                                    
                                                                return {ficha};
                                                            })}}
                                                        />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten2" id="cierreMaterial" type="text" value={this.state.ficha.cierreMaterial} onChange={(e) => {
                                                            let val = e.target.value;
                                                            this.setState(prevState => {
                                                                let ficha = Object.assign({}, prevState.ficha);
                                                                ficha.cierreMaterial = val;
                                                    
                                                                return {ficha};
                                                            })}}
                                                        />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="cierreColor" type="text" value={this.state.ficha.cierreColor} onChange={(e) => {
                                                            let val = e.target.value;
                                                            this.setState(prevState => {
                                                                let ficha = Object.assign({}, prevState.ficha);
                                                                ficha.cierreColor = val;
                                                    
                                                                return {ficha};
                                                            })}}
                                                        />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="pullerDetalle" type="text" value={this.state.ficha.pullerDetalle} onChange={(e) => {
                                                            let val = e.target.value;
                                                            this.setState(prevState => {
                                                                let ficha = Object.assign({}, prevState.ficha);
                                                                ficha.pullerDetalle = val;
                                                    
                                                                return {ficha};
                                                            })}}
                                                        />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten2" id="pullerMaterial" type="text" value={this.state.ficha.pullerMaterial} onChange={(e) => {
                                                            let val = e.target.value;
                                                            this.setState(prevState => {
                                                                let ficha = Object.assign({}, prevState.ficha);
                                                                ficha.pullerMaterial = val;
                                                    
                                                                return {ficha};
                                                            })}}
                                                        />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="pullerColor" type="text" value={this.state.ficha.pullerColor} onChange={(e) => {
                                                            let val = e.target.value;
                                                            this.setState(prevState => {
                                                                let ficha = Object.assign({}, prevState.ficha);
                                                                ficha.pullerColor = val;
                                                    
                                                                return {ficha};
                                                            })}}
                                                        />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="img2" class="col">
                                            <div>
                                                <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="labelImg">
                                                    <h5 id="codigoQR"><b>Imagen 2</b></h5>
                                                </label>
                                            </div>
                                            <div id="divImg1">
                                                <img></img>
                                            </div>
                                            <div class="text-center">
                                                <a href="">
                                                <input type="button" class="btn botonImg" style={{backgroundColor:'#a46f1f', color:'#ffffff' }} value="Cargar Imagen"></input></a>
                                            </div>                                       
                                        </div>
                                    </div><br/><br/>

                                    <div id="Espe3" class="row container">
                                        <div id="apartado3" class="col">
                                            <div id="Hilo" class="col">
                                                <div class="row">
                                                    <div class="col-3">
                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left"></label>
                                                        </div> 
                                                        <div id="cuero" class="form-group py-1">
                                                            <label style={{ color: "#A46F1F" }} class="control-label pull-left h5"><b>Puntera</b></label>
                                                        </div>

                                                        <div id="cuero" class="form-group py-1">
                                                            <label style={{ color: "#A46F1F" }} class="control-label pull-left h5"><b>Contrafuerte</b></label>
                                                        </div>                                                                                 
                                                    </div>

                                                    <div class="col contenedor1">
                                                        <div class="form-group py-1">
                                                            <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo">
                                                                <h5 id="codigoQR" class="col"><b>Marerial</b></h5>
                                                            </label>
                                                        </div>
                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <input id="punteraMaterial" class="form-control borde contra" type="text" value={this.state.ficha.punteraMaterial} onChange={(e) => {
                                                                    let val = e.target.value;
                                                                    this.setState(prevState => {
                                                                        let ficha = Object.assign({}, prevState.ficha);
                                                                        ficha.punteraMaterial = val;
                                                    
                                                                        return {ficha};
                                                                    })}}
                                                                />
                                                            </div>                                                
                                                        </div>

                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <input id="contraMaterial" class="form-control borde contra" type="text" value={this.state.ficha.contraMaterial} onChange={(e) => {
                                                                    let val = e.target.value;
                                                                    this.setState(prevState => {
                                                                        let ficha = Object.assign({}, prevState.ficha);
                                                                        ficha.contraMaterial = val;
                                                    
                                                                        return {ficha};
                                                                    })}}
                                                                />
                                                            </div>                                                       
                                                        </div><br/>                      
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3">
                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Piso</b></label>
                                                    </div>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Piso 1</label>
                                                    </div>
                                                    
                                                    <div id="cuero2" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Piso 2</label>
                                                    </div>

                                                    <div id="cuero3" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Plataforma</label>
                                                    </div>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Taco</label>
                                                    </div>                                         
                                                </div>

                                                <div class="col contenedor1">
                                                    <div class="form-group py-1">
                                                        <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                            <h5 id="codigoQR" class="col"><b>Material</b></h5>
                                                            <h5 id="codigoQR" class="col">Color<b></b></h5>
                                                            <h5 id="codigoQR" class="col"><b>Forrado</b></h5>
                                                        </label>
                                                    </div>
                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="p1Material" type="text" value={this.state.ficha.p1Material} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.p1Material = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten2" id="p1Color" type="text" value={this.state.ficha.p1Color} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.p1Color = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="p1Forrado" type="text" value={this.state.ficha.p1Forrado} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.p1Forrado = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="p2Material" type="text" value={this.state.ficha.p2Material} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.p2Material = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten2" id="p2Color" type="text" value={this.state.ficha.p2Color} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.p2Color = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="p2Forrado" type="text" value={this.state.ficha.p2Forrado} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.p2Forrado = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="plataformaMaterial" type="text" value={this.state.ficha.plataformaMaterial} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.plataformaMaterial = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten2" id="plataformaColor" type="text" value={this.state.ficha.plataformaColor} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.plataformaColor = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="plataformaForrado" type="text" value={this.state.ficha.plataformaForrado} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.plataformaForrado = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="tacoMaterial" type="text" value={this.state.ficha.tacoMaterial} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.tacoMaterial = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten2" id="tacoColor" type="text" value={this.state.ficha.tacoColor} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.tacoColor = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="tacoForrado" type="text" value={this.state.ficha.tacoForrado} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.tacoForrado = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="img3" class="col">
                                            <div>
                                                <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="labelImg" for="LastName">
                                                    <h5 id="codigoQR"><b>Imagen 3</b></h5>
                                                </label>
                                            </div>
                                            <div id="divImg1">
                                                <img></img>
                                            </div>
                                            <div class="text-center">
                                                <a href="">
                                                <input type="button" class="btn botonImg" style={{backgroundColor:'#a46f1f', color:'#ffffff' }} value="Cargar Imagen"></input></a>
                                            </div>                                       
                                        </div>
                                    </div><br/><br/>

                                    <div id="Espe4" class="row container">
                                        <div id="apartado4" class="col">
                                            <div id="Plantilla" class="col">
                                                <div class="row">
                                                    <div class="col-3">
                                                        <div id="cuero" class="form-group py-1">
                                                            <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Plantilla</b></label>
                                                        </div>

                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Plantilla</label>
                                                        </div>
                                                        
                                                        <div id="cuero2" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Acolche</label>
                                                        </div>                        
                                                    </div>

                                                    <div class="col contenedor1">
                                                        <div class="form-group py-1">
                                                            <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                                <h5 id="codigoQR" class="col"><b>Material</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Color</b></h5>
                                                            </label>
                                                        </div>
                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <input class="form-control borde" id="plantillaMaterial" type="text" value={this.state.ficha.plantillaMaterial} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.plantillaMaterial = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten1" id="plantillaColor" type="text" value={this.state.ficha.plantillaColor} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.plantillaColor = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                        </div>

                                                        <div class="form-group py-1">
                                                            <div class="">
                                                                <input class="form-control borde plantillaImp" id="acolcheMaterial" type="text" value={this.state.ficha.acolcheMaterial} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.acolcheMaterial = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                        </div>                                                   
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="costura" class="">
                                                <div class="row">
                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5"><b>Costura Plantilla</b></label>
                                                    </div>
                                                    <div class="col-3">
                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left"></label>
                                                        </div>
                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left">Hilo</label>
                                                        </div>
                                                        
                                                        <div id="cuero2" class="form-group py-1">
                                                            <label class="control-label pull-left">Costura</label>
                                                        </div>                                                                                      
                                                    </div>

                                                    <div class="col contenedor1">
                                                        <div class="form-group py-1">
                                                            <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                                <h5 id="codigoQR" class="col"><b>Tipo</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Número</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Color</b></h5>
                                                            </label>
                                                        </div>
                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <input class="form-control borde" id="h4Tipo" type="text" value={this.state.ficha.h4Tipo} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.h4Tipo = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten2" id="h4Numero" type="number" value={this.state.ficha.h4Numero} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.h4Numero = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten1" id="h4Color" type="text" value={this.state.ficha.h4Color} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.h4Color = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                        </div>

                                                        <div class="form-group py-1">
                                                            <div class="">
                                                                <input class="form-control borde CostAcceImp" id="costuraTipo" type="text" value={this.state.ficha.costuraTipo} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.costuraTipo = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                        </div>                      
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="Accesorios" class="">
                                                <div class="row">
                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Accesorios de Acabado</b></label>
                                                    </div>
                                                    <div class="col-3">
                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName"></label>
                                                        </div>

                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Sello</label>
                                                        </div>
                                                        
                                                        <div id="cuero2" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Cuerito</label>
                                                        </div>

                                                        <div id="cuero3" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Han Tag</label>
                                                        </div>                                                                                        
                                                    </div>

                                                    <div class="col contenedor1">
                                                        <div class="form-group py-1">
                                                            <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                                <h5 id="codigoQR" class="col"><b>Marca</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Tipo</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Material</b></h5>
                                                            </label>
                                                        </div>
                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <input class="form-control borde" id="selloMarca" type="text" value={this.state.ficha.selloMarca} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.selloMarca = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten2" id="selloTipo" type="text" value={this.state.ficha.selloTipo} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.selloTipo = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten1" id="selloMaterial" type="text" value={this.state.ficha.selloMaterial} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.selloMaterial = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                        </div>

                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <input class="form-control borde" id="cueritoMarca" type="text" value={this.state.ficha.cueritoMarca} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.cueritoMarca = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten2" id="cueritoTipo" type="text" value={this.state.ficha.cueritoTipo} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.cueritoTipo = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten1" id="cueritoMaterial" type="text" value={this.state.ficha.cueritoMaterial} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.cueritoMaterial = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                        </div>

                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <input class="form-control borde CostAcceImp" id="hantagMarca" type="text" value={this.state.ficha.hantagMarca} onChange={(e) => {
                                                                let val = e.target.value;
                                                                this.setState(prevState => {
                                                                    let ficha = Object.assign({}, prevState.ficha);
                                                                    ficha.hantagMarca = val;
                                                
                                                                    return {ficha};
                                                                })}}
                                                            />
                                                            </div>
                                                        </div><br/>                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="img4" class="col">
                                            <div>
                                                <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="labelImg" for="LastName">
                                                    <h5 id="codigoQR"><b>Imagen 4</b></h5>
                                                </label>
                                            </div>
                                            <div id="divImg1">
                                                <img id="divImg1"></img>
                                            </div>
                                            <div class="text-center">
                                                <a href="">
                                                <input type="button" class="btn botonImg" style={{backgroundColor:'#a46f1f', color:'#ffffff' }} value="Cargar Imagen"></input></a>
                                            </div>                                                                                 
                                        </div>
                                    </div><br/>
                                </div>
                            </div>                        
                        </div>
                        <div>
                            <div id="botonesFormDiv">
                                <div  class="row text-center container">
                                    <div class="col">
                                        <a id="margenbotom1" href="/#/fichas">
                                        <input type="button" class="btn botonesForm" style={{backgroundColor:'#696666', color:'#ffffff' }} value="Guardar y Volver" onClick={this.save}></input></a>
                                    </div>
                                    <div class="col">
                                        <a id="margenbotom2" href="/#/fichas">
                                        <input  type="button" class="btn botonesForm" style={{backgroundColor:'#696666', color:'#ffffff' }} value="Cancelar"></input></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form><br/><br/>
                </div>
            </div>
        )
    }
    
}
export default FormNuevo;