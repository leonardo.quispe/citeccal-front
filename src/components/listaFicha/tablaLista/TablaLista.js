import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faClipboardList, faFilePdf} from "@fortawesome/free-solid-svg-icons";
import './../../../assets/styles/nav-vertical.css';
import './../../../assets/styles/divs.css';
import logolat from './../../../assets/img/logolat.png';
import { ConsultaService } from '../../../service/ConsultaService';
import {Link} from 'react-router-dom';
import $ from 'jquery';
window.jQuery = $;


class TablaLista extends React.Component {
    constructor(props){
        super(props);
        this.state = ({
            fichas: [],
            pos: null,
            id: 0,
            linea: '',
            serie: '',
            alTaco: '',
            color: '',
            estilo: '',
            codHorma: '',
            codPlanta: ''
        });
        this.consultaService = new ConsultaService();
    }

    render(){
        return(
            <div style={{ backgroundColor:'#A46F1F'}} className="container-fluid vh-100">

                <div id="mostrar-nav" class="body"></div>
                    <nav className="nav-lat">
                        <div id="cerrar-nav"></div>
                        
                        <div><img src={logolat} className="logo"></img></div>
                        
                            <ul className="menu">
                                <li><a href="/#/home"><b>inicio</b></a></li>
                                <li><a href="/#/fichas"><b>Ficha Tecnica</b></a></li>
                                <li><a href="#"><b>Servicios</b></a></li>
                            </ul>
                    </nav>
                    
                    <br/>
                <div id="divTabla" style={{ backgroundColor:'#E5E5E5'}} class="container px-1 py-5">
                    <div class="d-flex container text-center">
                            <div class="p-2 container">
                                <div class="">
                                    <a href="/#/nuevaFicha">
                                    <input  type="button" class="btn" style={{backgroundColor:'#696666', color:'#ffffff'}} value="Nueva Ficha Tecnica"></input></a>
                                </div>
                            </div>
                        </div>

                    
                    <div class="container px-4">
                        <table class="table" id="tablaPag">
                            <thead class="table-dark text-center">
                                <tr>
                                    <th scope="col">CODIGO</th>
                                    <th scope="col">LINEA</th>
                                    <th scope="col">SERIE</th>
                                    <th scope="col">ALT. TACO</th>
                                    <th scope="col">COLOR</th>
                                    <th scope="col">ESTILO</th>
                                    <th scope="col">HORMA</th>
                                    <th scope="col">PLANTA</th>
                                    <th scope="col">OPCIONES</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                {this.state.fichas.map((ficha) => {
                                    return(
                                        <tr key={ficha.id}>
                                            <td>{ficha.id}</td>
                                            <td>{ficha.linea}</td>
                                            <td>{ficha.serie}</td>
                                            <td>{ficha.alTaco}</td>
                                            <td>{ficha.color}</td>
                                            <td>{ficha.estilo}</td>
                                            <td>{ficha.codHorma}</td>
                                            <td>{ficha.codPlanta}</td>
                                            <td>
                                                <Link to={`/fichas/mostrarFicha/${ficha.id}`}>
                                                    <button type="button" class="btn" style={{backgroundColor:'#FB9333'}}><FontAwesomeIcon icon={faClipboardList}/></button>
                                                </Link>
                                                {" "}
                                                <Link to={`/mostrarFicha/editarFicha/${ficha.id}`}>
                                                    <button type="button" class="btn" style={{backgroundColor:'#F9D620'}}><FontAwesomeIcon icon={faEdit}/></button>
                                                </Link>
                                                {" "}
                                                <button type="button" class="btn" style={{backgroundColor:'#F04646'}}><FontAwesomeIcon icon={faFilePdf}/></button>
                                            </td>
                                        </tr>
                                        );      
                                    })}
                            </tbody>
                        </table>                                                            
                    </div><br/>
                </div>
                
            </div>
            
        )
    }
    componentDidMount() {
        $('#mostrar-nav').on('click',function(){
            $('.nav-lat').toggleClass('mostrar');
        });
        $('#cerrar-nav').on('click',function(){
            $('.nav-lat').toggleClass('mostrar');
        });

        this.consultaService.getAll().then(data => this.setState({fichas: data}));
        
        $(document).ready(function(){
            $('#tablaPag').DataTable({
                language: {
                    processing: "Tratamiento en curso...",
                    search: "Buscar Ficha Tecnica &nbsp;:",
                    lengthMenu: "",
                    info: "_TOTAL_ Fichas Tecnicas",
                    infoEmpty: "No existen datos.",
                    infoFiltered: "(filtrado de _MAX_ elementos en total)",
                    infoPostFix: "",
                    loadingRecords: "Cargando...",
                    zeroRecords: "No se encontraron datos con tu busqueda",
                    emptyTable: "No hay datos disponibles en la tabla.",
                    paginate: {
                        first: "Primero",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Ultimo"
                    },
                    aria: {
                        sortAscending: ": active para ordenar la columna en orden ascendente",
                        sortDescending: ": active para ordenar la columna en orden descendente"
                    }
                },
                scrollY: 280,
                lengthMenu: [ [5, -1], [5] ],
                  
            });
        });
    }

    
}
export default TablaLista;