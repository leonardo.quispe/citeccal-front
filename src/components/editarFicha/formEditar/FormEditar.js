import React from 'react';
import './../../../assets/styles/nav-vertical.css';
import './../../../assets/styles/divs.css';
import logolat from './../../../assets/img/logolat.png';
import $ from 'jquery';
window.jQuery = $;

class FormNuevo extends React.Component {
    componentDidMount() {
        $('#mostrar-nav').on('click',function(){
            $('.nav-lat').toggleClass('mostrar');
        });
        $('#cerrar-nav').on('click',function(){
            $('.nav-lat').toggleClass('mostrar');
        });
    }
    render(){
        return(
            <div>
                <div id="mostrar-nav" class="body"></div>
                    <nav class="nav-lat">
                        <div id="cerrar-nav"></div>
                        
                        <div><img src={logolat} class="logo"></img></div>
                        
                            <ul class="menu">
                                <li><a href="/#/home"><b>inicio</b></a></li>
                                <li><a href="/#/fichas"><b>Ficha Tecnica</b></a></li>
                                <li><a href="#"><b>Servicios</b></a></li>
                            </ul>
                    </nav>
                <div>
                    <form>
                        <div id="general" class=""><br/><br/>
                            <div id="formGeneral">
                                <div id="divGeneral" class="row form-group">
                                    <div class="col">
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left" for="LastName">Codigo:</label>
                                            <div class="col">
                                                <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                            </div>
                                        </div>
                                        <div class="row orm-group">
                                            <label class="col control-label" for="FirstName">Linea:</label>
                                            <div class="col">
                                                <input class="form-control borde" id="FirstName" name="FirstName" type="text" value="" />
                                            </div>
                                        </div>
                                        <div class="row form-group py-2">
                                            <label class="col control-label pull-left" for="LastName">Serie:</label>
                                            <div class="col">
                                                <input class="form-control borde" id="LastName" name="LastName" type="text" value="" />
                                            </div>
                                        </div>
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left" for="LastName">Altura de Taco:</label>
                                            <div class="col">
                                                <input class="form-control borde" id="LastName" name="LastName" type="text" value="" />
                                            </div>
                                        </div>                                
                                    </div>

                                    <div class="col colum2">
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left" for="LastName">Color:</label>
                                            <div class="col">
                                                <input class="form-control borde" id="LastName" name="LastName" type="text" value="" />
                                            </div>
                                        </div>
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left" for="LastName">Estilo:</label>
                                            <div class="col">
                                                <input class="form-control borde" id="LastName" name="LastName" type="text" value="" />
                                            </div>
                                        </div>
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left" for="LastName">Codigo de Horma:</label>
                                            <div class="col">
                                                <input class="form-control borde" id="LastName" name="LastName" type="text" value="" />
                                            </div>
                                        </div>
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left" for="LastName">Codigo de Planta:</label>
                                            <div class="col">
                                                <input class="form-control borde" id="LastName" name="LastName" type="text" value="" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group py-1">                      
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><br/><br/>

                        <div id="formEspecificaciones">
                            <div>
                                <div id="" class="px-5 py-4 text-center">
                                    <h2 class="py-2" style={{ color: "#A46F1F" }} ><b>Especificaciones Tecnicas</b></h2>
                                </div><br/>

                                <div>
                                    <div id="Espe1" class="row container">
                                        <div id="apartado1" class="col">
                                            <div class="row">
                                                <div class="col-3">
                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Material</b></label>
                                                    </div>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Cuero 1</label>
                                                    </div>
                                                    
                                                    <div id="cuero2" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Cuero 2</label>
                                                    </div>

                                                    <div id="cuero3" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Cuero 3</label>
                                                    </div><br/>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Forro</b></label>
                                                    </div>
                                                    
                                                    <div id="cuero2" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Forro 1</label>
                                                    </div>

                                                    <div id="cuero3" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Forro 2</label>
                                                    </div>                                                
                                                </div>

                                                <div class="col contenedor1">
                                                    <div class="form-group py-1">
                                                        <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                            <h5 id="codigoQR" class="col"><b>Tipo</b></h5>
                                                            <h5 id="codigoQR" class="col"><b>Color</b></h5>
                                                        </label>
                                                    </div>
                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                    </div><br/><br/>

                                                    <div class="row form-group py-1 forro">
                                                        <div class="col">
                                                            <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="img1" class="col">
                                            <div>
                                                <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="labelImg" for="LastName">
                                                    <h5 id="codigoQR"><b>Imagen 1</b></h5>
                                                </label>
                                            </div>
                                            <div id="divImg1">
                                                <img></img>
                                            </div>
                                            <div class="text-center">
                                                <a href="">
                                                <input type="button" class="btn botonImg" style={{backgroundColor:'#a46f1f', color:'#ffffff' }} value="Cargar Imagen"></input></a>
                                            </div>                                       
                                        </div>
                                    </div><br/><br/>

                                    <div id="Espe2" class="row container">
                                        <div id="apartado2" class="col">
                                            <div id="Hilo" class="">
                                                <div class="row">
                                                    <div class="col-3">
                                                        <div id="cuero" class="form-group py-1">
                                                            <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Hilo</b></label>
                                                        </div>

                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Hilo 1</label>
                                                        </div>
                                                        
                                                        <div id="cuero2" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Hilo 2</label>
                                                        </div>

                                                        <div id="cuero3" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Hilo 3</label>
                                                        </div>                                                                                        
                                                    </div>

                                                    <div class="col contenedor1">
                                                        <div class="form-group py-1">
                                                            <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                                <h5 id="codigoQR" class="col"><b>Tipo</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Número</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Color</b></h5>
                                                            </label>
                                                        </div>
                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten2" id="" name="LastName" type="text" value="" />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                            </div>
                                                        </div>

                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten2" id="" name="LastName" type="text" value="" />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                            </div>
                                                        </div>

                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten2" id="" name="LastName" type="text" value="" />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                            </div>
                                                        </div><br/>                      
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3">
                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Accesorios</b></label>
                                                    </div>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Accesorios 1</label>
                                                    </div>
                                                    
                                                    <div id="cuero2" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Accesorios 2</label>
                                                    </div>

                                                    <div id="cuero3" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Accesorios 3</label>
                                                    </div><br/>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Cierre</b></label>
                                                    </div>
                                                    
                                                    <div id="cuero2" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Cierre</label>
                                                    </div>

                                                    <div id="cuero3" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Puller</label>
                                                    </div>                                                
                                                </div>

                                                <div class="col contenedor1">
                                                    <div class="form-group py-1">
                                                        <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                            <h5 id="codigoQR" class="col"><b>Detalle</b></h5>
                                                            <h5 id="codigoQR" class="col"><b>Material</b></h5>
                                                            <h5 id="codigoQR" class="col"><b>Color</b></h5>
                                                        </label>
                                                    </div>
                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten2" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten2" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten2" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                    </div><br/><br/>

                                                    <div class="row form-group py-1 forro">
                                                        <div class="col">
                                                            <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten2" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten2" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="img2" class="col">
                                            <div>
                                                <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="labelImg" for="LastName">
                                                    <h5 id="codigoQR"><b>Imagen 2</b></h5>
                                                </label>
                                            </div>
                                            <div id="divImg1">
                                                <img></img>
                                            </div>
                                            <div class="text-center">
                                                <a href="">
                                                <input type="button" class="btn botonImg" style={{backgroundColor:'#a46f1f', color:'#ffffff' }} value="Cargar Imagen"></input></a>
                                            </div>                                       
                                        </div>
                                    </div><br/><br/>

                                    <div id="Espe3" class="row container">
                                        <div id="apartado3" class="col">
                                            <div id="Hilo" class="col">
                                                <div class="row">
                                                    <div class="col-3">
                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName"></label>
                                                        </div> 
                                                        <div id="cuero" class="form-group py-1">
                                                            <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Contrafuerte</b></label>
                                                        </div>

                                                        <div id="cuero" class="form-group py-1">
                                                            <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Puntera</b></label>
                                                        </div>                                                                                 
                                                    </div>

                                                    <div class="col contenedor1">
                                                        <div class="form-group py-1">
                                                            <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo">
                                                                <h5 id="codigoQR" class="col"><b>Marerial</b></h5>
                                                            </label>
                                                        </div>
                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <input id="contra" class="form-control borde" name="LastName" type="text" value="" />
                                                            </div>                                                
                                                        </div>

                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <input id="contra" class="form-control borde" name="LastName" type="text" value="" />
                                                            </div>                                                        
                                                        </div><br/>                      
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3">
                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Piso</b></label>
                                                    </div>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Piso 1</label>
                                                    </div>
                                                    
                                                    <div id="cuero2" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Piso 2</label>
                                                    </div>

                                                    <div id="cuero3" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Plataforma</label>
                                                    </div>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Taco</label>
                                                    </div>                                         
                                                </div>

                                                <div class="col contenedor1">
                                                    <div class="form-group py-1">
                                                        <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                            <h5 id="codigoQR" class="col"><b>Material</b></h5>
                                                            <h5 id="codigoQR" class="col">Color<b></b></h5>
                                                            <h5 id="codigoQR" class="col"><b>Forrado</b></h5>
                                                        </label>
                                                    </div>
                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten2" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten2" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten2" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten2" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                        <div class="col">
                                                            <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="img3" class="col">
                                            <div>
                                                <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="labelImg" for="LastName">
                                                    <h5 id="codigoQR"><b>Imagen 3</b></h5>
                                                </label>
                                            </div>
                                            <div id="divImg1">
                                                <img></img>
                                            </div>
                                            <div class="text-center">
                                                <a href="">
                                                <input type="button" class="btn botonImg" style={{backgroundColor:'#a46f1f', color:'#ffffff' }} value="Cargar Imagen"></input></a>
                                            </div>                                       
                                        </div>
                                    </div><br/><br/>

                                    <div id="Espe4" class="row container">
                                        <div id="apartado4" class="col">
                                            <div id="Plantilla" class="col">
                                                <div class="row">
                                                    <div class="col-3">
                                                        <div id="cuero" class="form-group py-1">
                                                            <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Plantilla</b></label>
                                                        </div>

                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Plantilla</label>
                                                        </div>
                                                        
                                                        <div id="cuero2" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Acolche</label>
                                                        </div>                        
                                                    </div>

                                                    <div class="col contenedor1">
                                                        <div class="form-group py-1">
                                                            <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                                <h5 id="codigoQR" class="col"><b>Material</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Color</b></h5>
                                                            </label>
                                                        </div>
                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group py-1">
                                                            <div class="">
                                                                <input class="form-control borde" id="plantillaImp" name="LastName" type="text" value="" />
                                                            </div>
                                                        </div>                                                   
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="costura" class="">
                                                <div class="row">
                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Costura Plantilla</b></label>
                                                    </div>
                                                    <div class="col-3">
                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName"></label>
                                                        </div>
                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Hilo</label>
                                                        </div>
                                                        
                                                        <div id="cuero2" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Costura</label>
                                                        </div>                                                                                      
                                                    </div>

                                                    <div class="col contenedor1">
                                                        <div class="form-group py-1">
                                                            <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                                <h5 id="codigoQR" class="col"><b>Tipo</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Número</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Color</b></h5>
                                                            </label>
                                                        </div>
                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten2" id="" name="LastName" type="text" value="" />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group py-1">
                                                            <div class="">
                                                                <input class="form-control borde" id="CostAcceImp" name="LastName" type="text" value="" />
                                                            </div>
                                                        </div>                      
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="Accesorios" class="">
                                                <div class="row">
                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#A46F1F" }} class="control-label pull-left h5" for="LastName"><b>Accesorios de Acabado</b></label>
                                                    </div>
                                                    <div class="col-3">
                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName"></label>
                                                        </div>

                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Sello</label>
                                                        </div>
                                                        
                                                        <div id="cuero2" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Cuerito</label>
                                                        </div>

                                                        <div id="cuero3" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Han Tag</label>
                                                        </div>                                                                                        
                                                    </div>

                                                    <div class="col contenedor1">
                                                        <div class="form-group py-1">
                                                            <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="row margenTipo text-center">
                                                                <h5 id="codigoQR" class="col"><b>Marca</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Tipo</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Material</b></h5>
                                                            </label>
                                                        </div>
                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten2" id="" name="LastName" type="text" value="" />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                            </div>
                                                        </div>

                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <input class="form-control borde" id="" name="LastName" type="text" value="" />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten2" id="" name="LastName" type="text" value="" />
                                                            </div>
                                                            <div class="col">
                                                                <input class="form-control borde conten1" id="" name="LastName" type="text" value="" />
                                                            </div>
                                                        </div>

                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <input class="form-control borde" id="CostAcceImp" name="LastName" type="text" value="" />
                                                            </div>
                                                        </div><br/>                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="img4" class="col">
                                            <div>
                                                <label style={{backgroundColor:'#a46f1f', color:'#ffffff' }} class="labelImg" for="LastName">
                                                    <h5 id="codigoQR"><b>Imagen 4</b></h5>
                                                </label>
                                            </div>
                                            <div id="divImg1">
                                                <img id="divImg1"></img>
                                            </div>
                                            <div class="text-center">
                                                <a href="">
                                                <input type="button" class="btn botonImg" style={{backgroundColor:'#a46f1f', color:'#ffffff' }} value="Cargar Imagen"></input></a>
                                            </div>                                                                                 
                                        </div>
                                    </div><br/>
                                </div>
                            </div>                        
                        </div>
                        <div>
                            <div id="botonesFormDiv">
                                <div  class="row text-center container">
                                    <div class="col">
                                        <a id="margenbotom1" href="">
                                        <input type="button" class="btn botonesForm" style={{backgroundColor:'#696666', color:'#ffffff' }} value="Guardar y Volver"></input></a>
                                    </div>
                                    <div class="col">
                                        <a id="margenbotom2" href="">
                                        <input  type="button" class="btn botonesForm" style={{backgroundColor:'#696666', color:'#ffffff' }} value="Cancelar"></input></a>
                                    </div>
                                </div>
                            </div>
                        </div><br/>
                        <div class="botonesFormDiv1">
                            <div class="text-center container">
                                <a id="margenbotom3" href="">
                                <input  type="button" class="btn botonesForm1" style={{backgroundColor:'#696666', color:'#ffffff' }} value="Exportar a Pdf"></input></a>
                            </div>
                        </div>
                    </form><br/><br/>
                </div>
            </div>
        )
    }
}
export default FormNuevo;