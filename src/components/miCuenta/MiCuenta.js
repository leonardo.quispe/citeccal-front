import React from 'react';
import Menu from '../menu/Menu';
import MiPerfil from './miPerfil/MiPerfil';
import Footer from '../footer/Footer';

class MiCuenta extends React.Component {
    render(){
        return(
            <>
            <Menu/> 
			<main role="main" class="">
                <div class="">
				    <MiPerfil/>
                </div>	
	  		</main>
              <Footer />
	  		</>
        )
    }
}
export default MiCuenta;