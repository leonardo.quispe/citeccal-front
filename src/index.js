import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { HashRouter as Router, Route, Switch } from 'react-router-dom'; 
import reportWebVitals from './reportWebVitals';

//Estilos Bootstrap
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'; // Archivo CSS de Bootstrap
import '../node_modules/bootstrap/dist/js/bootstrap.min.js'; // Archivo Javascript de Bootstrap

import '../node_modules/datatables.net-dt/js/dataTables.dataTables';
import '../node_modules/datatables.net-dt/css/jquery.dataTables.min.css';
import '../node_modules/jquery/dist/jquery.min.js';


// Páginas del Sitio Web
import Login from './components/login/Login';
import Home from './components/home/Home';
import ListaFicha from './components/listaFicha/ListaFicha';
//import MiCuenta from './components/miCuenta/MiCuenta'; 
import NuevaFicha from './components/nuevaFicha/NuevaFicha';
import EditarFicha from './components/editarFicha/EditarFicha';
import MostrarFicha from './components/mostrarFicha/MostrarFicha';


// Configuración de la rutas del Sitio Web 
ReactDOM.render(
  	<Router>
	    <div>
	    	<Switch>
		        {/* Páginas */}
		        <Route exact path='/' component={Login} />
				<Route path='/home' component={Home} />
				<Route path='/mostrarFicha/editarFicha/:id'>
					<EditarFicha/>
				</Route>
				<Route path='/fichas/mostrarFicha/:id'>
					<MostrarFicha/>
				</Route>
				<Route path='/fichas' component={ListaFicha} />
				<Route path='/nuevaFicha' component={NuevaFicha} />
	      	</Switch>
	    </div>
    </Router>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
